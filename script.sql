USE [finDB]
GO
/****** Object:  Table [dbo].[IncomeBook]    Script Date: 11.10.2021 18:42:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IncomeBook](
	[Id] [int] NOT NULL,
	[CreationDate] [date] NULL,
	[Skin] [int] NULL,
	[TotalAmount] [decimal](18, 2) NULL,
	[Income] [decimal](18, 2) NULL,
 CONSTRAINT [PK_IncomeBook] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Pagination]    Script Date: 11.10.2021 18:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Pagination]
	(@PageNumber int,
	 @RowsOfPage int,
	 @Skin int,
	 @TotalAmountSum decimal OUTPUT,
	 @IncomeSum decimal OUTPUT)
AS

IF @Skin is null
	BEGIN
		SELECT * FROM IncomeBook ORDER BY (Id) 
		OFFSET (@PageNumber-1)*@RowsOfPage ROWS
		FETCH NEXT @RowsOfPage ROWS ONLY
		SELECT @TotalAmountSum = SUM(TotalAmount), @IncomeSum = SUM(Income) from IncomeBook
	END
ELSE
	BEGIN
		SELECT * FROM IncomeBook WHERE Skin = @Skin ORDER BY (Id)
		OFFSET (@PageNumber-1)*@RowsOfPage ROWS
		FETCH NEXT @RowsOfPage ROWS ONLY
		SELECT @TotalAmountSum = SUM(TotalAmount), @IncomeSum = SUM(Income) from IncomeBook WHERE Skin = @Skin
	END
GO
