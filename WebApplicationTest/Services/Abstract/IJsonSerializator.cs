﻿using System.Threading.Tasks;
using WebApplicationTest.Models;

namespace WebApplicationTest.Services.Abstract
{
    public interface IJsonSerializator
    {
        public Task<Result<string>> GenerateJsonAsync(int pageNumber, int rowsOfPage, int? skin);
    }
}
