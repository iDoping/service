﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.Models;
using WebApplicationTest.Services.Abstract;
using NLog;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System;
using System.Data;

namespace WebApplicationTest.Services
{
    public class JsonSerializator : IJsonSerializator
    {
        private readonly finDbContext _context;
        private readonly ILogger _logger;

        public JsonSerializator(ILogger logger, finDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Result<string>> GenerateJsonAsync(int pageNumber, int rowsOfPage, int? skin)
        {
            _logger.Info($"Start method GenerateJsonAsync with param pageNumber = {pageNumber}, rowsOfPage = {rowsOfPage}, skin = {skin}");
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {

                await connection.OpenAsync();

                var command = new SqlCommand("Pagination", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter[] sqlParams = new SqlParameter[]
                {
                   new SqlParameter ("@PageNumber", pageNumber ),
                   new SqlParameter ("@RowsOfPage",rowsOfPage ),
                   new SqlParameter ("@Skin", skin.HasValue ? skin.Value : DBNull.Value)
                };

                command.Parameters.AddRange(sqlParams);
                command.Parameters.Add("@IncomeSum", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                command.Parameters.Add("@TotalAmountSum", SqlDbType.Decimal).Direction = ParameterDirection.Output;

                var reader = await command.ExecuteReaderAsync();

                var incomeBookList = new List<IncomeBook>();
                var totalAmountSum = 0m;
                var incomeSum = 0m;

                while (await reader.ReadAsync())
                {
                    incomeBookList.Add(new IncomeBook
                    {
                        Id = int.Parse(reader["Id"].ToString()),
                        CreationDate = DateTime.Parse(reader["CreationDate"].ToString()),
                        Skin = int.Parse(reader["Skin"].ToString()),
                        TotalAmount = decimal.Parse(reader["TotalAmount"].ToString()),
                        Income = decimal.Parse(reader["Income"].ToString())
                    });
                }

                reader.Close();

                if (incomeBookList.Any())
                {
                    totalAmountSum = Convert.ToDecimal(command.Parameters["@TotalAmountSum"].Value);
                    incomeSum = Convert.ToDecimal(command.Parameters["@IncomeSum"].Value);

                    var data = new Linker
                    {
                        incomeBooks = incomeBookList.ToList(),
                        IncomeSum = incomeSum,
                        TotalAmountSum = totalAmountSum
                    };
                    return new Result<string> { RequestResult = JsonConvert.SerializeObject(data), Error = null };
                }
                else
                {
                    _logger.Error($"There are no data in database for skin = {skin}");
                    return new Result<string> { RequestResult = null, Error = "No Data" };
                }
            }
        }
    }
}