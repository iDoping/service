﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApplicationTest.Models;
using WebApplicationTest.Services.Abstract;
using NLog;

namespace WebApplicationTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SerializedTableController : ControllerBase
    {
        private readonly IJsonSerializator _jsonSerializator;
        private readonly ILogger _logger;

        public SerializedTableController(ILogger logger, IJsonSerializator jsonSerializator)
        {
            _logger = logger;
            _jsonSerializator = jsonSerializator;
        }

        [HttpGet(nameof(GetSerializedTable))]
        public async Task<Result<string>> GetSerializedTable(int pageNumber, int rowsOfPage, int? skin)
        {
            _logger.Info($"Get: {nameof(GetSerializedTable)}");
            return await _jsonSerializator.GenerateJsonAsync(pageNumber, rowsOfPage, skin);
        }
    }
}