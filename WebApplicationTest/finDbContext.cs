﻿using Microsoft.EntityFrameworkCore;
using WebApplicationTest.Models;

namespace WebApplicationTest
{
    public partial class finDbContext : DbContext
    {
        public finDbContext()
        {

        }

        public finDbContext(DbContextOptions<finDbContext> options) : base(options)
        {

        }
     
        public virtual DbSet<IncomeBook> IncomeBook { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
