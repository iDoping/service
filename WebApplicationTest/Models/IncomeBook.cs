﻿using System;

namespace WebApplicationTest.Models
{
    public class IncomeBook
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int Skin { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Income { get; set; }
    }
}
