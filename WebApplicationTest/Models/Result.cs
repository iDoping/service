﻿namespace WebApplicationTest.Models
{
    public class Result<T>
    {
        public T RequestResult { get; set; }
        public string Error { get; set; }
    }
}