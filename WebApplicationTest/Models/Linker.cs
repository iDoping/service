﻿using System.Collections.Generic;

namespace WebApplicationTest.Models
{
    public class Linker
    {
        public List<IncomeBook> incomeBooks { get; set; }
        public decimal TotalAmountSum { get; set; }
        public decimal IncomeSum { get; set; }
    }
}
